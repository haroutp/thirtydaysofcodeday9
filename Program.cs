﻿using System;

namespace Day9
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            System.Console.WriteLine(Factorial(n));
        }

        static int Factorial(int number){
            if(number == 1){
                return 1;
            }

            int total = number * Factorial(number - 1);

            return total;
        }
    }
}
